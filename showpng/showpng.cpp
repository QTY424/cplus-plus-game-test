﻿#include <easyx.h>
#include <iostream>
void drawImg(int x, int y, IMAGE* src)
{

    DWORD* pwin = GetImageBuffer();
    DWORD* psrc = GetImageBuffer(src);
    int win_w = getwidth();
    int win_h = getheight();
    int src_w = src->getwidth();
    int src_h = src->getheight();

    int real_w = (x + src_w > win_w) ? win_w - x : src_w;
    int real_h = (y + src_h > win_h) ? win_h - y : src_h;
    if (x < 0)
    {
        psrc += -x;
        real_w -= -x;
        x = 0;
    }
    if (y < 0)
    {
        psrc += (src_w * -y);
        real_h -= -y;
        y = 0;
    }
    pwin += (win_w * y + x);
    // 实现透明贴图
    for (int iy = 0; iy < real_h; iy++)
    {
        for (int ix = 0; ix < real_w; ix++)
        {
            byte a = (byte)(psrc[ix] >> 24); // 计
            if (a != 0)
            {
                pwin[ix] = psrc[ix];
            }
        }
        // 换到下一行
        pwin += win_w;
        psrc += src_w;
    }
}
int main()
{
	initgraph(640, 480, EX_SHOWCONSOLE);
	setbkcolor(RGB(230, 231, 232));
	cleardevice();

	//定义图片
	IMAGE img_mm;
	//加载图片
	loadimage(&img_mm, "assert/mm.jpg");
	//输出图片
	//putimage(0, 0, &img_mm);

	IMAGE img_plane;
	loadimage(&img_plane, "assert/c.png",100,100);
    drawImg(0, 0, &img_plane);





	//字体显示
	settextstyle(48,0,"微软雅黑");
	//设置背景模式
	setbkmode(TRANSPARENT);
	//显示出来
	outtextxy(10, 10, "你好");

















	getchar();
	closegraph();
	return 0;
}



