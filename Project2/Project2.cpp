﻿// Project2.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <easyx.h>
using namespace std;
//定义消息结构体变量
ExMessage msg = { 0 };
void drawShape()
{
	//画一个点
	putpixel(50, 50, RED);
	//设置线条颜色
	setlinecolor(BLUE);
	//设置线条样式
	setlinestyle(PS_SOLID, 10);
	//画一个线
	line(0, 0, getwidth(), getheight());

	//设置线条颜色
	setlinecolor(RED);
	//设置线条样式
	setlinestyle(PS_DASH, 10);
	//画一个矩形
	rectangle(50, 50, 150, 150);
	//设置填充颜色
	setfillcolor(YELLOW);
	fillrectangle(100, 100, 100 + 100, 100 + 100);
	//无边框填充矩形
	solidrectangle(400, 400, 500, 500);

	//绘制圆角矩形
	roundrect(200, 200, 300, 300, 50, 50);
	fillroundrect(300, 300, 350, 350, 20, 20);
	solidroundrect(350, 350, 400, 400, 20, 20);

	//画一个圆
	circle(700, 100, 20);
	fillcircle(600, 100, 20);
	solidcircle(500, 100, 20);
	clearcircle(400, 100, 20);

	//椭圆
	ellipse(0, 300, 0 + 100, 300 + 50);
	fillellipse(0, 400, 0 + 100, 400 + 50);
	solidellipse(0, 500, 0 + 100, 500 + 50);
	clearellipse(0, 600, 0 + 100, 600 + 50);

	//绘制折线条
	POINT points[] = { {0,0},{20,20},{50,50},{10,60},{100,100 } };
	setlinestyle(PS_DASHDOT, 5);
	polyline(points, 5);
}
void drawTxet()
{
	//设置文字大小
	settextstyle(48, 0, "微软雅黑");
	//设置文字颜色
	settextcolor(RED);
	//设置背景模式
	setbkmode(TRANSPARENT);
	//显示字体
	outtextxy(340, 100, "开始游戏");

	//输出int
	int score = 98;
	char str[50] = " ";
	sprintf(str, "score=%d", score);
	outtextxy(getwidth() - 200, 0, str);

}
void centerText()
{
	//集中显示
	int rx = 30, ry = 380, rw = 200, rh = 60;
	setbkcolor(RGB(255, 255, 255));
	fillrectangle(rx, ry, rx + rw, ry + rh);

	//绘制文字
	settextcolor(RGB(30, 30, 30));
	char str[] = "开始游戏";
	int hSpace = (rw - textwidth(str)) / 2;
	int vSpace = (rh - textheight(str)) / 2;
	outtextxy(rx + hSpace, ry + vSpace, str);
}
void getMouse()
{
	////定义消息结构体变量
	//ExMessage msg = { 0 };
	while (true) {	//获取消息 鼠标
		if (peekmessage(&msg, EX_MOUSE))
		{
			switch (msg.message)
			{
			case WM_LBUTTONDOWN:
				//左键按下
				cout << "鼠标左键按下pos(" << msg.x << "," << msg.y << ")" << endl;
				break;
			case WM_RBUTTONDOWN:
				//右键按下
				cout << "鼠标右键按下pos(" << msg.x << "," << msg.y << ")" << endl;
				break;
			case WM_MBUTTONDOWN:
				//中键按下
				cout << "鼠标中键按下pos(" << msg.x << "," << msg.y << ")" << endl;
				break;
			case WM_MOUSEWHEEL:
				//滚轮 120是向前 -120是向后
				cout << "滚轮方向dir(" << msg.wheel << ") 位置pos(" << msg.x << "," << msg.y << ")" << endl;
				break;
			case WM_LBUTTONDBLCLK:
				//左键双击
				cout << "左键双击pos(" << msg.x << "," << msg.y << ")" << endl;
				break;
			case WM_RBUTTONDBLCLK:
				//右键双击
				cout << "右键双击pos(" << msg.x << "," << msg.y << ")" << endl;
				break;
			case WM_MOUSEMOVE:
				cout << "鼠标移动pos(" << msg.x << "," << msg.y << ")" << endl;
				break;
			default:
				break;
			}
		}
	}
}

//mx,my是否在指定矩形区域
bool inArea(int mx, int my, int x, int y, int w, int h)
{
	if (mx > x && mx<x + w && my>y && my < y + h)
	{
		return true;
	}
	return false;
}
bool button(int x, int y, int w, int h, const char* text)
{
	//判断鼠标是否在按钮上
	if (inArea(msg.x, msg.y, x, y, w, h))
	{
		setfillcolor(RGB(93, 107, 153));
	}
	else
	{
		setfillcolor(RGB(230, 231, 232));
	}

	//绘制按钮
	//setfillcolor(RED);
	fillroundrect(x, y, x + w, y + h, 5, 5);

	//绘制按钮的文本
	settextcolor(RGB(30, 30, 30));
	int hSpace = (w - textwidth(text)) / 2;
	int vSpace = (h - textheight(text)) / 2;
	outtextxy(x + hSpace, y + vSpace, text);
	//判断按钮是否被点击
	if (msg.message == WM_LBUTTONDOWN && inArea(msg.x, msg.y, x, y, w, h))
	{
		return true;
	}
	return false;
}
void myButton()
{
	while (true) {
		if (peekmessage(&msg, EX_MOUSE))
		{

		}
		//双缓冲绘图 所有的绘图代码必须放在begin和end之间
		BeginBatchDraw();
		if (button(20, 20, 150, 35, "开始游戏")) {
			cout << "开始游戏" << endl;
		}
		if (button(200, 20, 150, 35, "退出游戏")) {
			cout << "退出游戏" << endl;
		}
		EndBatchDraw();
		//把消息清空
		msg.message = 0;
	}
}

void keybroad()
{
	while (true) {
		peekmessage(&msg, EX_KEY | EX_MOUSE);
		if (msg.message == WM_KEYDOWN)
		{
			//cout << "down" << endl;
			//具体判断
			switch (msg.vkcode) {
			case VK_UP:
				cout << "VK_UP" << endl;
				break;
			case VK_DOWN:
				cout << "VK_DOWN" << endl;
				break;
			case VK_LEFT:
				cout << "VK_LEFT" << endl;
				break;
			case VK_RIGHT:
				cout << "VK_RIGHT" << endl;
				break;
			case VK_SPACE:
				cout << "VK_SPACE" << endl;
				break;
			case 'A':
				cout << "A" << endl;
				break;
			case 'D':
				cout << "D" << endl;
				break;
			case 'S':
				cout << "S" << endl;
				break;
			case 'W':
				cout << "W" << endl;
				break;
			}
		}
		else if (msg.message == WM_KEYUP)
		{
			//cout << "up" << endl;
		}
		msg.message = 0;
	}
}


int main()
{
	//创建一个窗口
	initgraph(800, 640, EX_SHOWCONSOLE | EX_NOCLOSE | EX_NOMINIMIZE | EX_DBLCLKS);
	cout << "打开主界面成功" << endl;
	//设置窗口背景颜色
	//setbkcolor(WHITE);
	setbkcolor(RGB(198, 160, 223));

	//设置背景模式
	setbkmode(TRANSPARENT);
	//设置背景颜色填充整个窗口
	cleardevice();
	//画图
	//drawShape();
	//文字
	//drawTxet();
	//居中显示
	//centerText();

	//获取鼠标状态
	//getMouse();
	//按钮
	//myButton();
	
	//定义一个小球
	int x = 50; 
	int y = 50;
	int r = 20;
	int speed = 2;
	int vx = 0;
	int vy = 0;

	//按键消息
	//keybroad();

	while (true) {
		if (peekmessage(&msg, EX_KEY | EX_MOUSE))
		{

		}
		if (msg.message == WM_KEYDOWN) {
			switch (msg.vkcode) {
			case VK_UP:
				cout << "VK_UP" << endl;
				vy = -1;
				break;
			case VK_DOWN:
				cout << "VK_DOWN" << endl;
				vy = 1;
				break;
			case VK_LEFT:
				cout << "VK_LEFT" << endl;
				vx = -1;
				break;
			case VK_RIGHT:
				cout << "VK_RIGHT" << endl;
				vx = 1;
				break;
			}
}
		else if (msg.message == WM_KEYUP)
		{
			switch (msg.vkcode) {
			case VK_UP:
				cout << "VK_UP" << endl;
				vy = 0;
				break;
			case VK_DOWN:
				cout << "VK_DOWN" << endl;
				vy = 0;
				break;
			case VK_LEFT:
				cout << "VK_LEFT" << endl;
				vx = 0;
				break;
			case VK_RIGHT:
				cout << "VK_RIGHT" << endl;
				vx = 0;
				break;
			}
		}
		//双缓冲绘图 所有的绘图代码必须放在begin和end之间
		BeginBatchDraw();
		cleardevice();
		if (button(20, 20, 150, 35, "开始游戏")) {
			cout << "开始游戏" << endl;
		}
		if (button(200, 20, 150, 35, "退出游戏")) {
			cout << "退出游戏" << endl;
		}
		//绘制小球
		setfillcolor(RGB(43, 145, 175));
		solidcircle(x, y, r);
		//更新小球的位置
		x += speed * vx;
		y += speed * vy;
		EndBatchDraw();
		Sleep(10);
		//把消息清空
		msg.message = 0;
	}








	// 关闭图形窗口
	closegraph();

	return 0;
}

