﻿#include <easyx.h>
#include <iostream>
#include <time.h>
int main()
{
	initgraph(640, 480, EX_SHOWCONSOLE);
	setbkcolor(RGB(230, 231, 232));
	setbkmode(TRANSPARENT);
	cleardevice();
	settextcolor(BLACK);

	//FPS 60f
	const clock_t FPS = 1000 / 60;//1s/60帧=16.6  每一帧花费的时间
	int startTime = 0;
	int freamTime = 0;//当前帧实际执行的时间
	int score = 0;
	char str[50] = "";

	while (true)
	{
		startTime = clock();//程序运行到这行时的毫秒
		sprintf(str, "Score:%d", score++);
		//双缓冲防止闪频
		BeginBatchDraw();
		cleardevice();//用设置的背景颜色填充整个窗口
		outtextxy(20, 20, str);
		EndBatchDraw();

		freamTime = clock() - startTime;//程序执行时间
		if (freamTime > 0)
		{
			Sleep(FPS - freamTime);
		}
	}
}
