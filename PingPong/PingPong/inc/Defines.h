#pragma once

#ifndef _DEFINES_H_
#define _DEFINES_H_

#include <easyx.h>
//坐标
class Vector {
public:
	double x; 
	double y;
public:
	Vector() :x(0.0), y(0.0){}
	Vector(double x, double y) :x(x), y(y) {}
	bool operator== (const Vector & obj) const
	{
		return ((this->x == obj.x) && (this->y == obj.y));
	}
	bool operator!= (const Vector& obj) const
	{
		return !(*this == obj);
	}
	Vector operator+ (const Vector& obj) const
	{
		return { x + obj.x, y + obj.y };
	}
	Vector operator- (const Vector& obj) const
	{
		return { x - obj.x, y - obj.y };
	}
	Vector operator- () const
	{
		return { -x,-y };
	}
	friend Vector operator* (const Vector& obj, double num) 
	{
		return { obj.x * num,obj.y * num };
	}
	friend Vector operator* (double num,const Vector& obj )
	{
		return num*obj;
	}
	friend Vector operator/ (const Vector& obj, double num)
	{
		return { obj.x / num,obj.y / num };
	}
	friend Vector operator/ (double num, const Vector& obj)
	{
		return num / obj;
	}
};

//取别名
using Coordinate = Vector;

class Rect
{
public:
	union
	{
		Coordinate pos;
		struct { double x; double y; };
	};
	double width;
	double height;
	Rect() :x(0.0),y(0.0), width(0), height(0){}
	Rect(double x,double y,double width,double height):x(x),y(y),width(width),height(height){}
	Coordinate GetCenter() const
	{
		return { x + width * 0.5,y * height * 0.5 };
	}
	//碰撞检测 计算重合区域
	Coordinate Intersect(const Rect* rect) const
	{
		return Coordinate();
	}
	//判断点是不是在容器里面
	bool Contains(const Coordinate& coord) const
	{
		return ((x < coord.x && coord.x < x + width) && (y < coord.y && coord.y < y + height));
	}

	RECT ToEasyXRect() const
	{
		return { (LONG)x,(LONG)y,(LONG)(x + width),(LONG)(y + height) };
	}
};
#endif // !_DEFINES_H_
